import sqlite3

con = sqlite3.connect('DICCIONARIO.db')
print("conectado")
cursor = con.cursor()
try:
    con.execute('''CREATE TABLE DICCIONARIO
                (Nº INT PRIMARY KEY NOT NULL,
                PALABRA TEXT NOT NULL,
                SIGNIFICADO TEXT NOT NULL)''')
except Exception as e:
    print (e)

try:
    con.execute('''
        INSERT INTO DICCIONARIO (Nº, PALABRA, SIGNIFICADO)
        VALUES(1, 'Sopá', 'Se usa como saludo. "Sopá" es "pasó" pero invirtiendo las sílabas. También se escribe con x "¿qué xopá?"')
        ''')
    con.execute('''
        INSERT INTO DICCIONARIO (Nº, PALABRA, SIGNIFICADO)
        VALUES(2, 'Bulto', 'Un bulto es una persona que es mala realmente mala en lo que se desempeña. Un paso más allá (o tal vez dos o tres) en la falta de habilidades.')
        ''')
    con.execute('''
        INSERT INTO DICCIONARIO (Nº, PALABRA, SIGNIFICADO)
        VALUES(3, 'Mopri', '"Mopri" es "Primo" que no solo se refiere al hijo del tío de una persona sino que también describe a un amigo o pasiero.')
        ''')
    con.execute('''
        INSERT INTO DICCIONARIO (Nº, PALABRA, SIGNIFICADO)
        VALUES(4, 'Rantan', 'Cuando queremos decir que hay mucho de algo utilizamos la palabra rantan "hay rantan de carros en el estadio". Algunos sinónimos son buco y pocotón.')
        ''')
    con.commit()
except Exception as e:
    print (e)

cursor.execute('''
    SELECT Nº, PALABRA FROM DICCIONARIO
    ''')

for i in cursor:
    print ('Palabra', i)
print("................................................")

cursor.execute('''
    SELECT Nº, PALABRA, SIGNIFICADO FROM DICCIONARIO WHERE Nº=2
    ''')

for i in cursor:
    print ('Nº', i[0])
    print('PALABRA:    ', i[1])
    print('SIGNIFICADO:', i[2])
print("................................................")

cursor.execute('''
    DELETE FROM DICCIONARIO WHERE PALABRA='Bulto'
    ''')
con.commit()

cursor.execute('''
    UPDATE DICCIONARIO SET Nº=2 WHERE PALABRA='Mopri'
    ''')
con.commit()

cursor.execute('''
    UPDATE DICCIONARIO SET Nº=3 WHERE PALABRA='Rantan'
    ''')
con.commit()

con.close()
