import redis

r = redis.Redis()

r.hset("1", "Sopa", "Se usa como saludo. Sopa es paso pero invirtiendo las silabas. Tambien se escribe con x, que xopa")
r.hset("2", "Mopri", "Mopri es Primo que no solo se refiere al hijo del tio de una persona sino que tambien describe a un amigo o pasiero.")
r.hset("3", "Rantan", "Cuando queremos decir que hay mucho de algo utilizamos la palabra rantan, algunos sinonimos son buco y pocoton.")

print(r.hgetall("1"))
print(r.hgetall("2"))
print(r.hgetall("3"))
