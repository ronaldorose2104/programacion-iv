import pymongo
from pymongo import MongoClient

cluster = MongoClient("mongodb+srv://ronaldo:<contraseña>@cluster0.3oet5.mongodb.net/myFirstDatabase?retryWrites=true&w=majority")
db = cluster["DICCIONARIO"]
collection = db["Diccionario_MongoDB"]

P1 = {"_id": 1, "Nº": 1, "Palabra": "Sopá", "Significado": "Se usa como saludo. Sopá es pasó pero invirtiendo las sílabas. También se escribe con x , ¿qué xopá?"}
P2 = {"_id": 2, "Nº": 2, "Palabra": "Mopri", "Significado": "Mopri es Primo que no solo se refiere al hijo del tío de una persona sino que también describe a un amigo o pasiero."}
P3 = {"_id": 3, "Nº": 3, "Palabra": "Mopri", "Significado": "Mopri es Primo que no solo se refiere al hijo del tío de una persona sino que también describe a un amigo o pasiero."}
P4 = {"_id": 4, "Nº": 4, "Palabra": "Rantan", "Significado": "Cuando queremos decir que hay mucho de algo utilizamos la palabra rantan, algunos sinónimos son buco y pocotón."}

#collection.insert_one(P1)
#collection.insert_one(P2)
#collection.insert_one(P3)
#collection.insert_one(P4)

#results = collection.delete_one({"_id": 3})

#results = collection.update_one({"_id": 4}, {"$inc":{"Nº": -1}})


#results = collection.find({"Palabra": "Mopri"})

for result in results:
    print(result)
