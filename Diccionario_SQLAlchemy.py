import sqlalchemy as db

engine = db.create_engine('sqlite:///DICCIONARIO.db')
con = engine.connect()
metadata = db.MetaData()
DICCIONARIO = db.Table('DICCIONARIO', metadata, autoload=True, autoload_with=engine)

print(DICCIONARIO.columns.keys())
print(repr(metadata.tables['DICCIONARIO']))

query = db.select([DICCIONARIO])
resultado = con.execute(query).fetchall()
print(resultado)

query = db.select([DICCIONARIO]).where(DICCIONARIO.columns.Nº > 3)
resultado = con.execute(query).fetchall()
print(resultado)

query = db.select([DICCIONARIO]).order_by(db.asc(DICCIONARIO.columns.PALABRA))
resultado = con.execute(query).fetchall()
print(resultado)

DICCIONARIO.drop(engine)

